from os.path import dirname, abspath, join

from ramsis_hazard.core.schemas import \
    ScenarioSchema, HazardModelRunSchema
from ramsis_hazard.core.hazard_preparation_utils import \
    unique_timebins, name_scenario_directory, name_project_directory, \
    create_directory, get_model_weighting, get_results_for_timebin, \
    name_hazard_directory, \
    get_unique_model_configs, get_model_name_from_info, \
    create_model_weightings, create_inj_plan_info_list
from ramsis_hazard.core.hazard_file_preparation import \
    HazardRunFilePreparation
from ramsis_hazard.core.oq_run_utils import execute_haz_run
from ramsis_hazard.core.data_retrieval import get_forecastseries, \
    get_forecast, get_modelruns, get_modelrun_data


# set the path to the webservice
base_dir = "/home/sarsonl/repos/ramsis-hazard"
project_name = "forge_2022"
ws_url = 'http://127.0.0.1:8000/v1'
oq_url = 'http://localhost:8800'
forecastseries_id = 1

basedir = dirname(abspath(__file__))
job_config_template = join(basedir, "oq_config_template.ini")
source_model_template = join(basedir, "model_source_template.xml")
gsim_logic_tree = join(basedir, "gsim_logic_tree.xml")


forecastseries = get_forecastseries(forecastseries_id, ws_url)

forecast_lon, forecast_lat = forecastseries.geometryextent.centroid.coords[0]
print("The hazard will be calculated for the point: \n forecast longitude:",
      forecast_lon, "forecast latitude: ", forecast_lat)
print("The forecast id should be taken from the following forecast ids: "
      f"{forecastseries.forecasts}")


# One forecast has modelruns for different model configs and different
# injection plans
forecast_id = 1
# Get forecast info
forecast = get_forecast(forecast_id, ws_url)

# Get all the modelrun ids and the injectionplan ids associated.
modelruns = get_modelruns(forecast_id, ws_url)
modelconfigs = get_unique_model_configs(modelruns)

# Add weights to different models
# In this simple case, each model will have equal weighting
modelweightings = create_model_weightings(modelconfigs, source_model_template)

# Create iterable for modelruns and injection plans
modelrun_inj_plan_list = create_inj_plan_info_list(modelruns)

# Each injectionplan is a new 'scenario' and will output a set of OQ input
# files for a different hazard run
_, inj_plan_ids, _ = zip(*modelrun_inj_plan_list)
unique_inj_plan_ids = set(inj_plan_ids)

project_dir = name_project_directory(base_dir, project_name)


def create_hazard_files_for_scenario(
        inj_plan_id, lon, lat, minaltitude, maxaltitude,
        forecast_starttime, forecast_endtime, project_dir,
        modelrun_inj_plan_list, ws_url, modelweightings,
        gsim_logic_tree, job_config_template):
    timebins = []
    scenario_dir = name_scenario_directory(project_dir, inj_plan_id)
    scenario = ScenarioSchema(
        id=inj_plan_id,
        haz_runs=[])
    # collect model runs for inj plans
    modelrun_ids = [item[0] for item in modelrun_inj_plan_list
                    if item[1] == inj_plan_id]

    for modelrun_id in modelrun_ids:
        resulttimebins = get_modelrun_data(modelrun_id, ws_url)
        model_name = get_model_name_from_info(
            modelrun_inj_plan_list, modelrun_id)
        assert model_name

        for result in resulttimebins:
            result.modelname = model_name
            model_weighting = get_model_weighting(model_name, modelweightings)
            result.weight = model_weighting.weight
            result.sourcemodeltemplate = model_weighting.sourcemodeltemplate

        timebins.extend(resulttimebins)
    forecast_timebins = unique_timebins(
        timebins, forecast_starttime, forecast_endtime)
    # print(f"Unique timebins found for forecast: {timebins}")
    # create files for hazard model run for each timebin.
    for haz_run_id, (starttime, endtime) in enumerate(forecast_timebins):
        model_timebins = get_results_for_timebin(
            resulttimebins, starttime, endtime)

        haz_run = HazardModelRunSchema(**dict(
            id=haz_run_id,
            describedinterval_start=starttime,
            describedinterval_end=endtime,
            gsimlogictree=gsim_logic_tree,
            jobconfigtemplate=job_config_template,
            sourcemodelfiles=[]))
        haz_dir = name_hazard_directory(
            scenario_dir, starttime, endtime, haz_run_id)
        create_directory(haz_dir)
        print("created haz directory: ", haz_dir)
        haz_run.oqinputdir = haz_dir
        output_dir = join(haz_dir, "outputs")
        create_directory(output_dir)

        file_preparation = HazardRunFilePreparation(
            model_timebins, haz_run, lat,
            lon, minaltitude, maxaltitude, output_dir)
        file_preparation.create_all()
        scenario.haz_runs.append(haz_run)
    return scenario


# Create hazard model run files per scenario
# where a scenario is defined by an injection plan.
scenarios = []
for inj_plan_id in unique_inj_plan_ids:
    scenario = create_hazard_files_for_scenario(
        inj_plan_id, forecast_lon, forecast_lat, forecastseries.minaltitude,
        forecastseries.maxaltitude, forecast.starttime, forecast.endtime,
        project_dir, modelrun_inj_plan_list, ws_url, modelweightings,
        gsim_logic_tree, job_config_template)
    scenarios.append(scenario)

print(f"Number of scenarios: {len(scenarios)}")

for scenario in scenarios:
    for haz_run in scenario.haz_runs:
        execute_haz_run(haz_run, scenario.id, oq_url)
        exit()
