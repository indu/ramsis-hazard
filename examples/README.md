# ramsis hazard Forge 2022 example

The data from the FORGE 2022 stimulation on 21st April can be explored and visualized using the Jupyter Notebook `forge_2022.ipynb` in this folder.

To install you can run the following commands:

```bash
# clone the repo and move to this current folder
git clone https://gitlab.seismo.ethz.ch/indu/ramsis-hazard.git

# the notebooks require python 3.10 or over

# update and install the necessary libraries
# For development purposes it is recommended to install in editable mode
pip install -e .

# run jupyter and then open the notebook in the browser
jupyter notebook

```

The code requires the openquake engine to be running
at a know address. You should add this address to the
oq_url variable.
If you require a local installation, then you can 
set it up as follows:

This does not need to be in the same environment:
```bash
pip install openquake.engine
oq webui start
``` 

