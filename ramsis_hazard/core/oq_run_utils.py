import time

from ramsis_hazard.core.worker import OQHazardWorkerHandle
from ramsis_hazard.core.schemas import HazardModelRunSchema
from ramsis_hazard.config.config import get_settings


def execute_haz_run(haz_run, inj_plan_id, oq_url, wait_time=5.0):
    # make sure there is a small delay in submitting openquake
    # runs just so system doesn't get overloaded.
    # TODO move this to the jupyter notebook, waiting
    # should not take place here.
    executor = OQHazardModelRunExecutor(
        haz_run, oq_url, inj_plan_id)
    run_id = executor.run()
    time.sleep(wait_time)
    return run_id


class OQHazardModelRunExecutor:

    TASK_ACCEPTED = 'created'

    def __init__(self, hazard_model_run: HazardModelRunSchema,
                 url: str, inj_plan_id: int):
        self.hazard_model_run = hazard_model_run
        self.url = url
        self.inj_plan_id = inj_plan_id
        self.settings = get_settings()

    def run(self):
        _worker_handle = OQHazardWorkerHandle.from_info(
            self.url, self.hazard_model_run.id, self.inj_plan_id)

        try:
            # sarsonl bad work around for OpenQuake bug just to test hazard
            resp = _worker_handle.compute(
                self.settings.JOB_CONFIG_FILENAME,
                self.settings.LOGIC_TREE_SOURCE_MODEL_FILENAME,
                self.settings.GSIM_LOGIC_TREE_FILENAME,
                self.hazard_model_run.sourcemodelfiles,
                self.hazard_model_run.oqinputdir)
        except OQHazardWorkerHandle.RemoteWorkerError as err:
            raise Exception(
                "model run submission has failed with "
                "error: RemoteWorkerError. Check if remote worker is"
                " accepting requests.") from err

        status = resp['status']

        if status != self.TASK_ACCEPTED:
            raise Exception(f"model run {resp['id']} "
                            f"has returned an error: {resp}")

        self.hazard_model_run.runid = resp['job_id']
        return self.hazard_model_run.runid
