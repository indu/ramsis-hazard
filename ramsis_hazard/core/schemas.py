import json
import uuid
from datetime import datetime
from typing import Any, Callable, List, Optional, Type

from pydantic import BaseConfig, BaseModel, create_model, validator
from pydantic.utils import GetterDict
from shapely import from_geojson
from shapely.geometry import Polygon
from sqlalchemy.inspection import inspect

BaseConfig.arbitrary_types_allowed = True
BaseConfig.orm_mode = True


def real_value_factory(quantity_type: type) -> Type[BaseModel]:
    _func_map = dict([
        ('value', (Optional[quantity_type], None)),
        ('uncertainty', (Optional[float], None)),
        ('loweruncertainty', (Optional[float], None)),
        ('upperuncertainty', (Optional[float], None)),
        ('confidencelevel', (Optional[float], None)),
        ('pdfvariable', (Optional[List[float]], None)),
        ('pdfprobability', (Optional[List[float]], None)),
        ('pdfbinedges', (Optional[List[float]], None))
    ])

    retval = create_model(
        f'Real{quantity_type.__name__}',
        __config__=BaseConfig,
        **_func_map)
    return retval


class ValueGetter(GetterDict):
    def get(self, key: str, default: Any) -> Any:
        # if the key-col mapping is 1:1 just return the value
        if hasattr(self._obj, key):
            return getattr(self._obj, key, default)

        # get this SQLAlchemy objects' column names.
        inspected = inspect(type(self._obj))
        cols = [c.name for c in inspected.columns]
        cols += inspected.relationships.keys()

        # else it's probably a sub value
        # get all column names which are present for this key
        elem = [k for k in cols if k.startswith(f'{key}_')]
        if elem:
            # create a dict for the sub value
            return_dict = {}
            for k in elem:
                return_dict[k.partition(
                    '_')[-1]] = getattr(self._obj, k, default)
            return return_dict
        else:
            return default


class RealFloatValue(real_value_factory(float)):
    pass


class RealDatetimeValue(real_value_factory(datetime)):
    pass


def creationinfo_factory() -> Type[BaseModel]:
    _func_map = dict([
        ('author', (Optional[str], None)),
        ('agencyid', (Optional[str], None)),
        ('creationtime', (Optional[datetime], None)),
        ('version', (Optional[str], None)),
        ('copyrightowner', (Optional[str], None)),
        ('licence', (Optional[str], None)),
    ])
    return create_model('CreationInfo', __config__=BaseConfig, **_func_map)


class CreationInfo(creationinfo_factory()):
    pass


def create_validator(field: str, method: Callable) -> classmethod:
    decorator = validator(field, allow_reuse=True, pre=True)
    vld = decorator(method)
    return vld


def create_geom(v, values) -> Polygon:
    # This seems to be called more than once
    # when parsing, so check the type before.
    if isinstance(v, dict):
        geom = from_geojson(json.dumps(v))
        return geom
    return v


class SeismicRateSchema(BaseModel):
    id: int
    lat: float
    lon: float
    altitude: float
    numberevents: RealFloatValue
    b: RealFloatValue
    a: RealFloatValue
    alpha: RealFloatValue
    mc: RealFloatValue
    weight: Optional[float]

    class Config:
        getter_dict = ValueGetter


class SeismicEventSchema(BaseModel):
    id: int
    x: RealFloatValue
    y: RealFloatValue
    z: RealFloatValue
    magnitude: RealFloatValue
    datetime: RealDatetimeValue

    class Config:
        getter_dict = ValueGetter


class HydraulicSampleSchema(BaseModel):
    id: int
    datetime: Optional[RealDatetimeValue]
    bottomtemperature: Optional[RealFloatValue]
    bottomflow: Optional[RealFloatValue]
    bottompressure: Optional[RealFloatValue]
    toptemperature: Optional[RealFloatValue]
    topflow: Optional[RealFloatValue]
    toppressure: Optional[RealFloatValue]
    fluiddensity: Optional[RealFloatValue]
    fluidviscosity: Optional[RealFloatValue]
    fluidph: Optional[RealFloatValue]
    fluidcomposition: Optional[str]

    class Config:
        getter_dict = ValueGetter


class SeismicCatalogSchema(BaseModel):
    id: int
    events: Optional[List[SeismicEventSchema]]


class HydraulicsSchema(BaseModel):
    samples: Optional[List[HydraulicSampleSchema]]


class WellSectionSchema(BaseModel):
    id: int
    publicid: str
    starttime: datetime
    endtime: Optional[datetime]
    topx: Optional[RealFloatValue]
    topy: Optional[RealFloatValue]
    topz: Optional[RealFloatValue]
    bottomx: Optional[RealFloatValue]
    bottomy: Optional[RealFloatValue]
    bottomz: Optional[RealFloatValue]
    bottommeasureddepth: Optional[RealFloatValue]
    holediameter: Optional[RealFloatValue]
    casingdiameter: Optional[RealFloatValue]
    topclosed: Optional[bool]
    bottomclosed: Optional[bool]
    sectiontype: Optional[str]
    description: Optional[str]
    injectionplan: Optional[HydraulicsSchema]
    hydraulics: Optional[HydraulicsSchema]


class InjectionWellSchema(BaseModel):
    id: int
    publicid: str
    altitude: RealFloatValue
    longitude: RealFloatValue
    latitude: RealFloatValue
    bedrockdepth: RealFloatValue
    sections: Optional[List[WellSectionSchema]]

    class Config:
        getter_dict = ValueGetter


class ProjectSchema(BaseModel):
    id: int
    name: Optional[str]
    creationinfo: Optional[CreationInfo]
    description: Optional[str]
    starttime: Optional[datetime]
    endtime: Optional[datetime]
    forecastseries: Optional[List[int]]

    class Config:
        getter_dict = ValueGetter


class ForecastSeriesSchema(BaseModel):
    id: int
    creationinfo: Optional[CreationInfo]
    starttime: Optional[datetime]
    endtime: Optional[datetime]
    name: Optional[str]
    minaltitude: float
    maxaltitude: float
    geometryextent: Optional[Polygon]
    forecastinterval: Optional[float]
    forecastduration: Optional[float]
    forecasts: Optional[List[int]]

    _validate_geom = validator(
        "geometryextent",
        pre=True,
        always=True,
        allow_reuse=True)(create_geom)


class ForecastSchema(BaseModel):
    id: int
    creationinfo: Optional[CreationInfo]
    name: Optional[str]
    starttime: Optional[datetime]
    endtime: Optional[datetime]
    runs: Optional[List[int]]
    status: Optional[str]

    class Config:
        getter_dict = ValueGetter


class ModelConfigSchema(BaseModel):
    id: int
    name: Optional[str]
    url: Optional[str]
    sfm_module: Optional[str]
    sfm_class: Optional[str]
    config: Optional[dict]
    last_updated: Optional[datetime]

    @validator("name", pre=True, always=True)
    def validate_date(cls, value):
        return value.replace(" ", "_")


class ModelRunSchema(BaseModel):
    id: int
    runid: uuid.UUID
    status: Optional[str]
    modelconfig: Optional[ModelConfigSchema]
    resulttimebins: Optional[List[int]]
    injectionplan_id: Optional[int]


class SeismicPredictionGridSchema(BaseModel):
    id: Optional[int]
    seismicrates: Optional[List[SeismicRateSchema]]


class ResultTimeBinSchema(BaseModel):
    id: int
    starttime: datetime
    endtime: datetime
    seismicpredictiongrids: Optional[List[SeismicPredictionGridSchema]]
    seismiccatalogs: Optional[List[SeismicCatalogSchema]]
    # Each timeresultbin has a modelname associated
    # so that we can find the weighting
    modelname: Optional[str]
    weight: Optional[float]
    sourcemodeltemplate: Optional[str]


class SeismicityModelWeightingSchema(BaseModel):
    # this is the same information that will be copied to the
    # ResultTimeBinSchema but we need this object as a reference
    # so that the details can be looked up
    # for individual time bins.
    id: int
    modelname: str
    # This is the weight compared to other models, can be any +ve number.
    weight: float
    sourcemodeltemplate: str


class HazardModelRunSchema(BaseModel):
    id: int
    runid: Optional[int]
    describedinterval_start: datetime
    describedinterval_end: datetime
    oqinputdir: Optional[str]
    jobconfigtemplate: str
    gsimlogictree: str
    sourcemodelfiles: Optional[List[str]]
    outputdir: Optional[str]


class ScenarioSchema(BaseModel):
    id: int
    haz_runs: Optional[List[HazardModelRunSchema]]
