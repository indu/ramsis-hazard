import configparser
from datetime import datetime
from typing import Any
import logging
from os.path import join

import shutil
from ramsis_hazard.filewriters.source_model import SourceModelFileWriter, \
    rate_info
from ramsis_hazard.filewriters.source_model_logic_tree import \
    SourceModelLogicTreeFileWriter, ModelWeighting
from ramsis_hazard.filewriters.job_ini import \
    JobIniFileWriter
from ramsis_hazard.config.config import get_settings
from ramsis_hazard.core.schemas import ResultTimeBinSchema, \
    HazardModelRunSchema, SeismicRateSchema

logger = logging.getLogger(__name__)

DATETIME_FORMAT = '%Y-%m-%dT-%H-%M'


class SourceModelInfoStore(dict):

    def add(self, key: str, value: Any):
        """Add value to list stored under key."""
        if key not in self:
            self[key] = list()
        # TODO add validation of value here.
        self[key].append(value)


class SourceModelInfoCreation:
    def __init__(self, start: datetime, end: datetime, model_name: str,
                 proj_string: str, max_mag: float,
                 seismicity_model_basename: str, minaltitude: float,
                 maxaltitude: float):
        self.start = start
        self.end = end
        self.model_name = model_name
        self.max_mag = max_mag
        self.minaltitude = minaltitude
        self.maxaltitude = maxaltitude
        self.seismicity_model_basename = seismicity_model_basename
        self.context_store = SourceModelInfoStore()

    def source_model_name(self, grid_id: int) -> str:
        # This id must be unique for every source area/point in the
        # openquake xml files, even if the areas are identical.

        return self.seismicity_model_basename.format(
            self.model_name, self.start.strftime(DATETIME_FORMAT),
            self.end.strftime(DATETIME_FORMAT),
            grid_id)

    def real_value_geometry_result(self, rate: SeismicRateSchema,
                                   grid_id: int):
        rate_info_data = rate_info(
            rate.a.value, rate.b.value, rate.mc.value,
            rate.lat, rate.lon, self.minaltitude, self.maxaltitude,
            self.start, self.end,
            self.max_mag, grid_id)
        source_model_file_name = self.source_model_name(grid_id)
        self.context_store.add(source_model_file_name, rate_info_data)

    def create_context_list(self, timebin: ResultTimeBinSchema):

        if not timebin.seismicpredictiongrids:
            raise NotImplementedError(
                "Results must have Gutenberg-Richter "
                "value, catalogs not supported "
                "currently.")
        for grid in timebin.seismicpredictiongrids:
            for seismicrate in grid.seismicrates:
                self.real_value_geometry_result(seismicrate, grid.id)


class HazardRunFilePreparation:
    SEISMICITY_MODEL_BASENAME = '{}_source_{}_{}_gridid_{}.xml'

    def __init__(self, result_time_bins: ResultTimeBinSchema,
                 hazard_model_run: HazardModelRunSchema,
                 lat: float, lon: float, minaltitude: float,
                 maxaltitude: float, output_dir):
        self.result_time_bins = result_time_bins
        self.hazard_model_run = hazard_model_run
        self.source_model_weights = None
        self.source_model_filenames = []
        self.settings = get_settings()
        self.lat = lat
        self.lon = lon
        self.minaltitude = minaltitude
        self.maxaltitude = maxaltitude
        self.output_dir = output_dir

    def create_source_model_writer(
            self):

        oq_input_file_directory = self.hazard_model_run.oqinputdir
        # TODO need to use the start and end to weight the results if not
        # already weighted
        start = self.hazard_model_run.describedinterval_start
        end = self.hazard_model_run.describedinterval_end

        max_mag = 5.0
        self.source_model_weights = list()

        # Find the normalization factor so that all the weights from seismicity
        # models will add up to 1.0.

        source_files = []
        # total weight contributed by the different model timebins
        # Used for normalization of the rate weights. All rate weights per
        # Hazard run must equal 1
        timebin_weights = sum([t.weight for t in self.result_time_bins])
        for timebin in self.result_time_bins:
            number_rates = sum([len(g.seismicrates) for g
                                in timebin.seismicpredictiongrids])
            rate_weight = timebin.weight / number_rates
            normalized_weight = rate_weight / timebin_weights
            template_xml = timebin.sourcemodeltemplate
            # TODO need to find what finction we actually need to use, as
            # I think we don't want to use a max_mag at all,
            # instead incremental
            # rates. Can OQ just not handle this?
            file_writer = SourceModelFileWriter(
                timebin.modelname, oq_input_file_directory,
                template_xml, max_mag)

            # Currently just handle samples, not catalogs
            results_converter = SourceModelInfoCreation(
                start, end, timebin.modelname, "", max_mag,
                self.SEISMICITY_MODEL_BASENAME,
                self.minaltitude, self.maxaltitude)
            results_converter.create_context_list(timebin)

            for source_model_file_name, geom_info_list in \
                    results_converter.context_store.items():
                self.source_model_filenames.append(source_model_file_name)
                file_writer.write(source_model_file_name, geom_info_list)
                self.hazard_model_run.sourcemodelfiles.append(
                    source_model_file_name)
                self.source_model_weights.append(
                    ModelWeighting(source_model_file_name, normalized_weight))
            source_files.append(results_converter.source_model_name)

    def create_source_model_logic_tree(self):

        if not self.source_model_weights:
            raise Exception("Please run the create_source_model_writer "
                            "before this method.")
        source_model_logic_tree_writer = SourceModelLogicTreeFileWriter(
            self.hazard_model_run.oqinputdir)
        source_model_logic_tree_writer.write(
            self.settings.LOGIC_TREE_SOURCE_MODEL_FILENAME,
            self.source_model_weights)

    def create_gsim(self):
        shutil.copyfile(self.hazard_model_run.gsimlogictree,
                        join(self.hazard_model_run.oqinputdir,
                             self.settings.GSIM_LOGIC_TREE_FILENAME))

    def create_job_ini(self):
        config = configparser.ConfigParser()
        config.read(self.hazard_model_run.jobconfigtemplate)
        if 'calculation' not in config.sections():
            config.add_section('calculation')

        config['calculation']['source_model_logic_tree_file'] = \
            self.settings.LOGIC_TREE_SOURCE_MODEL_FILENAME
        config['calculation']['gsim_logic_tree_file'] = \
            self.settings.GSIM_LOGIC_TREE_FILENAME

        # OQ requires the investigation time to be 1.0 year
        # Scaling in time can be done with postprocessing
        config['calculation']['investigation_time'] = "1"
        config['general']['sites'] = f"{self.lon} {self.lat}"
        config['output']['export_dir'] = self.output_dir
        job_ini_file_writer = JobIniFileWriter(
            self.hazard_model_run.oqinputdir)
        job_ini_file_writer.write(self.settings.JOB_CONFIG_FILENAME, config)

    def create_all(self):
        self.create_gsim()
        self.create_source_model_writer()
        self.create_source_model_logic_tree()
        self.create_job_ini()
