import os
from datetime import datetime
from typing import List, Tuple
from collections import namedtuple
from pathlib import Path

from ramsis_hazard.core.schemas import ResultTimeBinSchema, \
    SeismicityModelWeightingSchema, ModelConfigSchema, \
    ModelRunSchema

import logging
logger = logging.getLogger(__name__)

DATETIME_FORMAT = '%Y-%m-%dT-%H-%M'

HazardRunInfo = namedtuple(
    "HazardRunInfo",
    "hazard_model_run_id hazard_dir source_model_filenames")


def create_directory(dir_string: str):
    Path(dir_string).mkdir(parents=True, exist_ok=True)


def name_project_directory(parent_dir: str, project_name: str):
    project_name = project_name.replace(" ", "")
    project_dir = os.path.join(
        parent_dir, f"Project_{project_name}")
    return project_dir


def name_scenario_directory(project_dir: str, inj_plan_id: int) -> str:
    scenario_dir = os.path.join(
        project_dir,
        f"InjectionScenario_{inj_plan_id}")
    return scenario_dir


def name_hazard_directory(
        parent_dir: str,
        starttime: datetime, endtime: datetime, haz_id: int) -> str:
    hazard_dir = os.path.join(
        parent_dir, f"HazardModelRunId_{haz_id}_"
        f"{starttime.strftime(DATETIME_FORMAT)}_"
        f"{endtime.strftime(DATETIME_FORMAT)}")
    return hazard_dir


def unique_timebins(timebins_full_list: List[ResultTimeBinSchema],
                    forecast_starttime: datetime,
                    forecast_endtime: datetime
                    ) -> List[Tuple[datetime, datetime]]:
    timebins = []
    for timebin in timebins_full_list:
        # check that we don't want to run hazard on calibration output also
        if timebin.starttime >= forecast_starttime and \
                timebin.endtime <= forecast_endtime:
            timebins.append((timebin.starttime, timebin.endtime))
    unique_timebins = list(set(timebins))
    return sorted(unique_timebins)


def get_results_for_timebin(timebins: List[ResultTimeBinSchema],
                            starttime: datetime,
                            endtime: datetime) -> List[ResultTimeBinSchema]:
    return [timebin for timebin in timebins if
            timebin.starttime == starttime and timebin.endtime == endtime]


def get_model_weighting(
        model_name: str,
        model_weighting_list: List[SeismicityModelWeightingSchema]) -> \
        SeismicityModelWeightingSchema:
    for model_weighting in model_weighting_list:
        if model_weighting.modelname == model_name:
            return model_weighting
    raise Exception(f"Model not found! {model_name}, {model_weighting_list}")


def get_unique_model_configs(modelruns: List[ModelRunSchema]) -> \
        List[ModelConfigSchema]:
    unique_names = []
    unique_configs = []
    for run in modelruns:
        if run.modelconfig.name not in unique_names:
            unique_names.append(run.modelconfig.name)
            unique_configs.append(run.modelconfig)
    return unique_configs


def get_model_name_from_info(model_info: List[Tuple[int, int, str]],
                             modelrun_id: int) -> str:
    for model_run_id, inj_plan_id, model_name in model_info:
        if model_run_id == modelrun_id:
            return model_name


def create_model_weightings(
        modelconfigs: List[ModelConfigSchema],
        model_source_template) -> SeismicityModelWeightingSchema:
    # The weights of each model do not have to add up to 1,
    # This is simply a relative weighting that will be normalized later.
    modelweightings = []
    # TODO The below function should ideally get read from a config file so
    # that the weightings can be non-equal.
    for config in modelconfigs:
        modelweighting = SeismicityModelWeightingSchema(**dict(
            id=config.id,
            modelname=config.name,
            weight=1.0,
            sourcemodeltemplate=model_source_template))
        modelweightings.append(modelweighting)
    return modelweightings


def create_inj_plan_info_list(modelruns: ModelRunSchema):
    modelrun_inj_plan_list = []
    for modelrun in modelruns:
        modelrun_inj_plan_list.append([modelrun.id, modelrun.injectionplan_id,
                                       modelrun.modelconfig.name])
    return modelrun_inj_plan_list
