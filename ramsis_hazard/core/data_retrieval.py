
from pydantic import parse_obj_as
from typing import List
import requests
from ramsis_hazard.core.schemas import \
    ForecastSchema, ForecastSeriesSchema, ModelRunSchema, \
    ResultTimeBinSchema


def get_data(base_url: str, path: str):
    r = requests.get(f'{base_url}{path}')
    return r.json()


def get_forecastseries(forecastseries_id: int, ws_url: str) \
        -> ForecastSeriesSchema:
    forecastseries_dict = get_data(
        ws_url, f"/forecastseries/{forecastseries_id}")
    return ForecastSeriesSchema(**forecastseries_dict)


def get_forecast(forecast_id: int, ws_url: str) \
        -> ForecastSchema:
    forecast_dict = get_data(
        ws_url, f"/forecasts/{forecast_id}")
    # return parse_obj_as(ForecastSchema, forecast_dict)
    return ForecastSchema(**forecast_dict)


def get_modelruns(forecast_id: int, ws_url: str) \
        -> ForecastSchema:
    modelrun_list = get_data(
        ws_url, f"/forecasts/{forecast_id}/modelruns")
    return parse_obj_as(List[ModelRunSchema], modelrun_list)


def get_modelrun_data(modelrun_id: int, ws_url: str) \
        -> ResultTimeBinSchema:
    results_list = get_data(
        ws_url, f"/modelruns/{modelrun_id}/results")
    return parse_obj_as(List[ResultTimeBinSchema], results_list)
