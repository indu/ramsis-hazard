from pydantic import BaseSettings
from functools import lru_cache


class Settings(BaseSettings):
    LOGIC_TREE_SOURCE_MODEL_FILENAME = 'logic_tree_model_sources.xml'
    GSIM_LOGIC_TREE_FILENAME = "gsim_logic_tree.xml"
    JOB_CONFIG_FILENAME = "oq_config.ini"

    class Config:
        env_file = ".env"


@lru_cache()
def get_settings():
    return Settings()
