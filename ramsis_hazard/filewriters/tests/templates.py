
source_model_template_xml = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '<nrml xmlns:gml="http://www.opengis.net/gml" xmlns="http://openquake.org/xmlns/nrml/0.4">\n' # noqa
    '  <sourceModel name="{{ model_name }}">\n'
    '    {% for subgeom in geometries %}\n'
    '    <areaSource tectonicRegion="Active Shallow Crust" id="{{ subgeom.area_source_id }}" name="{{ subgeom.area_source_name }}">\n' # noqa
    '      <areaGeometry>\n'
    '        <gml:Polygon>\n'
    '          <gml:exterior>\n'
    '            <gml:LinearRing>\n'
    '              <gml:posList>{{ subgeom.linearring }}</gml:posList>\n' # noqa
    '            </gml:LinearRing>\n'
    '          </gml:exterior>\n'
    '        </gml:Polygon>\n'
    '        <upperSeismoDepth>0.0</upperSeismoDepth>\n'
    '        <lowerSeismoDepth>30.0</lowerSeismoDepth>\n'
    '      </areaGeometry>\n'
    '      <magScaleRel>WC1994</magScaleRel>\n'
    '      <ruptAspectRatio>1.0</ruptAspectRatio>\n'
    '      <truncGutenbergRichterMFD\n'
    '        aValue="{{ subgeom.a_value }}"\n'
    '        bValue="{{ subgeom.b_value }}"\n'
    '        maxMag="{{ subgeom.max_mag }}"\n'
    '        minMag="{{ subgeom.min_mag }}"/>\n'
    '    <nodalPlaneDist>\n'
    '        <nodalPlane probability="1.0" strike="0.0" dip="90.0" rake="0.0" />\n' # noqa
    '    </nodalPlaneDist>\n'
    '      <hypoDepthDist>\n'
    '        <hypoDepth probability="0.25" depth="1.0" /> \n'
    '        <hypoDepth probability="0.25" depth="2.0" />\n'
    '        <hypoDepth probability="0.25" depth="3.0" />\n'
    '        <hypoDepth probability="0.25" depth="4.0" />\n'
    '      </hypoDepthDist>\n'
    '    </areaSource>\n'
    '  {% endfor %}\n'
    '  </sourceModel>\n'
    '</nrml>\n')

gsim_logic_tree_template = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '\n'
    '<nrml xmlns:gml="http://www.opengis.net/gml"\n'
    '      xmlns="http://openquake.org/xmlns/nrml/0.4">\n'
    '    <logicTree logicTreeID="lt1">\n'
    '\n'
    '            <logicTreeBranchSet uncertaintyType="gmpeModel" branchSetID="bs1"\n' # noqa
    '                    applyToTectonicRegionType="Active Shallow Crust">\n'
    '\n'
    '                <logicTreeBranch branchID="b1">\n'
    ' <uncertaintyModel>BooreAtkinson2008</uncertaintyModel>\n'
    '                    <uncertaintyWeight>1.0</uncertaintyWeight>\n'
    '                </logicTreeBranch>\n'
    '\n'
    '            </logicTreeBranchSet>\n'
    '\n'
    '    </logicTree>\n'
    '</nrml>\n')

job_config_dict = dict(
    general=dict(
        description="rt-ramsis Geldinganes configuraiton",
        calculation_mode="classical",
        random_seed=1024,
        region="-5.0000000E-01 -5.0000000E-01, -3.0000000E-01 -1.0000000E-01, 1.0000000E-01 2.0000000E-01, 3.0000000E-01 -8.0000000E-01", # noqa
        region_grid_spacing=1),
    logic_tree=dict(
        number_of_logic_tree_samples=0),
    erf=dict(
        rupture_mesh_spacing=50,
        width_of_mfd_bin=0.1,
        area_source_discretization=10),
    site_params=dict(
        reference_vs30_type="measured",
        reference_vs30_value=760.0),
    calculation=dict(
        source_model_logic_tree_file="logic_tree_model_sources.xml",
        gsim_logic_tree_file="gmpe_logic_tree.xml",
        investigation_time=1,
        intensity_measure_types_and_levels={"PGA": [0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 0.75]}, # noqa
        truncation_level=3,
        maximum_distance=100.0),
    output=dict(
        export_dir="outputs/1111",
        mean_hazard_curves=True,
        quantile_hazard_curves="0.1 0.5 0.9",
        uniform_hazard_spectra=False,
        poes="0.01 0.05 0.2 0.5"))
