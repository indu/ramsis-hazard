from ramsis_hazard.filewriters.source_model import\
    SourceModelFileWriter, SubgeometrySampleParameters
from ramsis_hazard.filewriters.tests.templates import source_model_template_xml


output_xml = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '<nrml xmlns:gml="http://www.opengis.net/gml" xmlns="http://openquake.org/xmlns/nrml/0.4">\n' # noqa
    '  <sourceModel name="mymodel">\n'
    '    \n'
    '    <areaSource tectonicRegion="Active Shallow Crust" id="1" name="area_source_1">\n' # noqa
    '      <areaGeometry>\n'
    '        <gml:Polygon>\n'
    '          <gml:exterior>\n'
    '            <gml:LinearRing>\n'
    '              <gml:posList>-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01</gml:posList>\n' # noqa
    '            </gml:LinearRing>\n'
    '          </gml:exterior>\n'
    '        </gml:Polygon>\n'
    '        <upperSeismoDepth>0.0</upperSeismoDepth>\n'
    '        <lowerSeismoDepth>30.0</lowerSeismoDepth>\n'
    '      </areaGeometry>\n'
    '      <magScaleRel>WC1994</magScaleRel>\n'
    '      <ruptAspectRatio>1.0</ruptAspectRatio>\n'
    '      <truncGutenbergRichterMFD\n'
    '        aValue="1.0"\n'
    '        bValue="2.0"\n'
    '        maxMag="5.0"\n'
    '        minMag="0.2"/>\n'
    '    <nodalPlaneDist>\n'
    '        <nodalPlane probability="1.0" strike="0.0" dip="90.0" rake="0.0" />\n' # noqa
    '    </nodalPlaneDist>\n'
    '      <hypoDepthDist>\n'
    '        <hypoDepth probability="0.25" depth="1.0" /> \n'
    '        <hypoDepth probability="0.25" depth="2.0" />\n'
    '        <hypoDepth probability="0.25" depth="3.0" />\n'
    '        <hypoDepth probability="0.25" depth="4.0" />\n'
    '      </hypoDepthDist>\n'
    '    </areaSource>\n'
    '  \n'
    '    <areaSource tectonicRegion="Active Shallow Crust" id="2" name="area_source_2">\n' # noqa
    '      <areaGeometry>\n'
    '        <gml:Polygon>\n'
    '          <gml:exterior>\n'
    '            <gml:LinearRing>\n'
    '              <gml:posList>-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01</gml:posList>\n' # noqa
    '            </gml:LinearRing>\n'
    '          </gml:exterior>\n'
    '        </gml:Polygon>\n'
    '        <upperSeismoDepth>0.0</upperSeismoDepth>\n'
    '        <lowerSeismoDepth>30.0</lowerSeismoDepth>\n'
    '      </areaGeometry>\n'
    '      <magScaleRel>WC1994</magScaleRel>\n'
    '      <ruptAspectRatio>1.0</ruptAspectRatio>\n'
    '      <truncGutenbergRichterMFD\n'
    '        aValue="1.0"\n'
    '        bValue="2.0"\n'
    '        maxMag="5.0"\n'
    '        minMag="0.2"/>\n'
    '    <nodalPlaneDist>\n'
    '        <nodalPlane probability="1.0" strike="0.0" dip="90.0" rake="0.0" />\n' # noqa
    '    </nodalPlaneDist>\n'
    '      <hypoDepthDist>\n'
    '        <hypoDepth probability="0.25" depth="1.0" /> \n'
    '        <hypoDepth probability="0.25" depth="2.0" />\n'
    '        <hypoDepth probability="0.25" depth="3.0" />\n'
    '        <hypoDepth probability="0.25" depth="4.0" />\n'
    '      </hypoDepthDist>\n'
    '    </areaSource>\n'
    '  \n'
    '    <areaSource tectonicRegion="Active Shallow Crust" id="3" name="area_source_3">\n' # noqa
    '      <areaGeometry>\n'
    '        <gml:Polygon>\n'
    '          <gml:exterior>\n'
    '            <gml:LinearRing>\n'
    '              <gml:posList>-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01</gml:posList>\n' # noqa
    '            </gml:LinearRing>\n'
    '          </gml:exterior>\n'
    '        </gml:Polygon>\n'
    '        <upperSeismoDepth>0.0</upperSeismoDepth>\n'
    '        <lowerSeismoDepth>30.0</lowerSeismoDepth>\n'
    '      </areaGeometry>\n'
    '      <magScaleRel>WC1994</magScaleRel>\n'
    '      <ruptAspectRatio>1.0</ruptAspectRatio>\n'
    '      <truncGutenbergRichterMFD\n'
    '        aValue="1.0"\n'
    '        bValue="2.0"\n'
    '        maxMag="5.0"\n'
    '        minMag="0.2"/>\n'
    '    <nodalPlaneDist>\n'
    '        <nodalPlane probability="1.0" strike="0.0" dip="90.0" rake="0.0" />\n' # noqa
    '    </nodalPlaneDist>\n'
    '      <hypoDepthDist>\n'
    '        <hypoDepth probability="0.25" depth="1.0" /> \n'
    '        <hypoDepth probability="0.25" depth="2.0" />\n'
    '        <hypoDepth probability="0.25" depth="3.0" />\n'
    '        <hypoDepth probability="0.25" depth="4.0" />\n'
    '      </hypoDepthDist>\n'
    '    </areaSource>\n'
    '  \n'
    '  </sourceModel>\n'
    '</nrml>')


class TestSourceModelWriter:
    def test_simple_write(self, tmpdir):
        mydir = tmpdir.mkdir("mydir")
        filename = "sourcemodel.xml"
        myfilename = mydir.join(filename)
        input_info = [
            SubgeometrySampleParameters(
                "-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01", 1.0, 2.0, 0.2, 5.0, 1, "area_source_1"), # noqa
            SubgeometrySampleParameters(
                "-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01", 1.0, 2.0, 0.2, 5.0, 2, "area_source_2"), # noqa
            SubgeometrySampleParameters(
                "-5.0E-01 -5.0E-01 -3.0E-01 -1.0E-01 1.0E-01 2.0E-01 3.0E-01 -8.0E-01", 1.0, 2.0, 0.2, 5.0, 3, "area_source_3")] # noqa
        lt_writer = SourceModelFileWriter("mymodel", mydir,
                                          source_model_template_xml,
                                          input_info)
        lt_writer.write(filename, input_info)
        print(myfilename.read())
        assert myfilename.read() == output_xml
