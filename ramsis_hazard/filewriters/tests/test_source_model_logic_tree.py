from ramsis_hazard.filewriters.source_model_logic_tree import\
    SourceModelLogicTreeFileWriter, ModelWeighting

output_xml = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '<nrml xmlns:gml="http://www.opengis.net/gml"\n'
    '      xmlns="http://openquake.org/xmlns/nrml/0.5">\n'
    '    <logicTree logicTreeID="lt1">\n'
    '        <logicTreeBranchingLevel branchingLevelID="bl1">\n'
    '            <logicTreeBranchSet uncertaintyType="sourceModel" branchSetID="bs1">\n' # noqa
    '                \n'
    '                <logicTreeBranch branchID="1">\n'
    '                    <uncertaintyModel>model1.xml</uncertaintyModel>\n'
    '                    <uncertaintyWeight>0.4</uncertaintyWeight>\n'
    '                </logicTreeBranch>\n'
    '                  \n'
    '                <logicTreeBranch branchID="2">\n'
    '                    <uncertaintyModel>model2.xml</uncertaintyModel>\n'
    '                    <uncertaintyWeight>0.4</uncertaintyWeight>\n'
    '                </logicTreeBranch>\n'
    '                  \n'
    '                <logicTreeBranch branchID="3">\n'
    '                    <uncertaintyModel>model3.xml</uncertaintyModel>\n'
    '                    <uncertaintyWeight>0.2</uncertaintyWeight>\n'
    '                </logicTreeBranch>\n'
    '                  \n'
    '            </logicTreeBranchSet>\n'
    '        </logicTreeBranchingLevel>\n'
    '    </logicTree>\n'
    '</nrml>')


class TestLogicTreeWriter:
    def test_simple_write(self, tmpdir):
        mydir = tmpdir.mkdir("mydir")
        filename = "myfile.xml"
        myfilename = mydir.join(filename)
        input_info = [
            ModelWeighting("model1.xml", 0.4),
            ModelWeighting("model2.xml", 0.4),
            ModelWeighting("model3.xml", 0.2)]
        lt_writer = SourceModelLogicTreeFileWriter(mydir)
        lt_writer.write(filename, input_info)
        assert myfilename.read() == output_xml
