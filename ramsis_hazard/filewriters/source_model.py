import os
from collections import namedtuple
from datetime import datetime
from typing import List
from math import log10
from ramsis_hazard.filewriters.utils import render_template_from_string

RateParameters = namedtuple(
    "RateParameters",
    "lat lon mindepth maxdepth a_value b_value min_mag max_mag area_source_id")


def rate_info(
        a_value: float, b_value: float, mc_value: int,
        lat, lon, minaltitude, maxaltitude,
        start: datetime, end: datetime,
        max_mag: float, area_source_id: int):
    if not mc_value or mc_value < 2.5:
        mc_value = 2.5
        # Does the range of depth over which we are running the seismicity
        # models feed into the vertical space we are running hazard for?
        # We have this information as input in case we need to use it.
        # But for now, just run over a stardard depth.
        mindepth = 0.0
        maxdepth = 30.0
    b_value_log10 = log10(b_value)

    return RateParameters(
        lat, lon, mindepth, maxdepth, a_value, b_value_log10,
        mc_value, max_mag, area_source_id)


class SourceModelFileWriter:
    """
    Class to write xml source model file. One class to be instantiated
    per model config, so can be reused for different epochs and realizations
    from the same model run.
    """
    def __init__(self, model_name, oq_input_file_directory,
                 template_xml, max_mag):

        self.model_name = model_name
        self.oq_input_file_directory = oq_input_file_directory
        with open(template_xml, 'r') as ofile:
            self.template_xml = ofile.read()
        self.max_mag = max_mag

    def write(self, file_name: str,
              rate_info: List[RateParameters]):
        model_context = dict(
            model_name=self.model_name,
            resultgrid=rate_info)

        with open(
            os.path.join(
                self.oq_input_file_directory,
                file_name), 'w') as source_model_file:
            source_model_file.write(render_template_from_string(
                self.template_xml,
                model_context))
