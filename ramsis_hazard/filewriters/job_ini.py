import os


class JobIniFileWriter:

    def __init__(self, oq_input_file_directory):
        self.oq_input_file_directory = oq_input_file_directory

    def write(self, file_name, job_ini_config_parser):
        with open(
            os.path.join(
                self.oq_input_file_directory,
                file_name), 'w') as job_ini_file:
            job_ini_config_parser.write(job_ini_file)
