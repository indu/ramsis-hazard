from jinja2 import Environment, FileSystemLoader

import logging

logger = logging.getLogger(__name__)


def render_template_from_string(template_xml, context):
    template_environment = Environment(
        autoescape=False,
        trim_blocks=False)
    return template_environment.from_string(template_xml).render(context)


def render_template_from_file(template_filename, context, path):
    template_environment = Environment(
        autoescape=False,
        loader=FileSystemLoader(path),
        trim_blocks=False)
    return template_environment.get_template(
        template_filename).render(context)
