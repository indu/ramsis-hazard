
import os
from collections import namedtuple
from ramsis_hazard.filewriters.utils import render_template_from_string

dirpath = os.path.dirname(os.path.abspath(__file__))
parent_dirpath = os.path.dirname(dirpath)

ModelWeighting = namedtuple(
    "ModelWeighting",
    "source_model_file_name weight")

# A list of ModelWeighting tuples is used to populate
# This xml string.
xml_string = (
    '<?xml version="1.0" encoding="UTF-8"?>\n'
    '<nrml xmlns:gml="http://www.opengis.net/gml"\n'
    '      xmlns="http://openquake.org/xmlns/nrml/0.5">\n'
    '    <logicTree logicTreeID="lt1">\n'
    '        <logicTreeBranchingLevel branchingLevelID="bl1">\n'
    '            <logicTreeBranchSet uncertaintyType="sourceModel" branchSetID="bs1">\n' # noqa
    '                {% for info in source_model_info %}\n'
    '                <logicTreeBranch branchID="{{ loop.index }}">\n'
    '                    <uncertaintyModel>{{ info.source_model_file_name }}</uncertaintyModel>\n' # noqa
    '                    <uncertaintyWeight>{{ info.weight }}</uncertaintyWeight>\n' # noqa
    '                </logicTreeBranch>\n'
    '                  {% endfor %}\n'
    '            </logicTreeBranchSet>\n'
    '        </logicTreeBranchingLevel>\n'
    '    </logicTree>\n'
    '</nrml>\n')


class SourceModelLogicTreeFileWriter:
    def __init__(self, oq_input_file_directory,
                 logic_tree_template_xml=xml_string):
        self.oq_input_file_directory = oq_input_file_directory
        self.template_xml = logic_tree_template_xml

    def write(self, file_name, source_model_info):
        """
        file_name: str filename (no path) to write to.
        source_model_info: list of ModelWeighting tuples
        """
        logic_tree_context = dict(
            source_model_info=source_model_info)
        with open(
            os.path.join(
                self.oq_input_file_directory,
                file_name), 'w') as source_model_logic_tree_file:
            template = render_template_from_string(
                self.template_xml,
                logic_tree_context)
            source_model_logic_tree_file.write(template)
